<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S03: Activity</title>
</head>
<body>

	<h2>Person</h2>
	<p><?= $person->printName(); ?></p>

	<h2>Developer</h2>
	<p><?= $developer->printName(); ?></p>

	<h2>Engineer</h2>
	<p><?= $engineer->printName(); ?></p>


	<h2>Volts Member</h2>
	<p><?= var_dump($member1); ?></p>
	<p><?= var_dump($member2); ?></p>
	<p><?= var_dump($member3); ?></p>
	<p><?= var_dump($member4); ?></p>
	<p><?= var_dump($member5); ?></p>

	<p><?= $voltsmember1->printName(); ?></p>
	<p><?= $voltsmember2->printName(); ?></p>
	<p><?= $voltsmember3->printName(); ?></p>
	<p><?= $voltsmember4->printName(); ?></p>
	<p><?= $voltsmember5->printName(); ?></p>

</body>
</html>
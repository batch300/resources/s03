<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S03:Classes and Objects</title>
</head>
<body>

	<h1>Objects from Variable</h1>
	<p><?php echo $buildingObj->name; ?></p>
	<p><?php echo $buildingObj->address->city; ?></p>
	<p><?= print_r($buildingObj)?></p>
	<p><?php echo $buildingObj2->address; ?></p>

	<h1>Objects from Classes</h1>
	<p><?php var_dump($building)?></p>

	<h2>Modify the Instantiated Object</h2>
	<?php $building->name = "GMA Network";?>
	<?php $building->floors = "Twenty-one";?>
	<p><?php var_dump($building)?></p>
	<p><?= $building->printName();?></p>

	<h1>Inheritance (Condominium Object)</h1>
	<p><?php var_dump($condominium)?></p>
	<p><?= $condominium->name;?></p>
	<p><?= $condominium->floors;?></p>
	<p><?= $building->address;?></p>

	<h1>Polymorphism (Overriding the behavopr of the printName() method</h1>
	<p><?= $condominium->printName();?></p>

	<h1>Pokemon</h1>
	<p><?php var_dump($pokemon1)?></p>
	<p><?php var_dump($pokemon2)?></p>
	<p><?php var_dump($pokemon3)?></p>

	
</body>
</html>
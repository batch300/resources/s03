<?php

//[1] Objects as Variables

$buildingObj = (object)[

	'name' => 'Caswynn Building',
	'floors' => 8,
	'address' => (object)[
		'barangay' => 'Sacred Heart',
		'city'=>'Quezon City',
		'country'=>'Philippines'
	]
];

$buildingObj2 = (object)[
	"name" => "GMA Network",
	"floors" => 20,
	"address" => "EDSA corner Timog Avenue, Diliman, Quezon City"
];

//[2] Objects from Classes

class Building {

	//this represents a class named Building

	//1.Properties
	//Characteristics of an object
	public $name;
	public $floors;
	public $address;

	//2. Constructor Function
	//the class also has a constructor method __construct() that accepts parameters to initialize the properties of the object
	
	public function __construct($name,$floors,$address){
		/*
			"$this" keyword refers to the properties and methods within the class scope
			"$this->name" is accessing the "name" property of the current class (Building) and assigning the value of the $name upon instantiation of an object
		*/
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	//3. Methods
	//These are functions inside of an object that can perform a specific action

	public function printName(){
		return "The name of this building is $this->name";
	}
}

//Instantiating Building Class to create a new building object

$building = new Building("Caswynn Building",8,"Timog Avenue, Quezon City, Philippines");


//[3] Inheritance and Polymorphism
//Inheritance - the derived classes are allowed to inherit properties and methods from a specified base class
//The "extends" keyword is used to inherit the properties and methods of a base or parent class

//Parent class => Building
//Child class => Condominium

Class Condominium extends Building{

	//the building properties and methods are inherited in this class

	//Polymorphism - methods inherited by th derived class can be overidden to have a behavior different from the method of the base class


	public function printName(){
		return "The name of this condominium is $this->name";
	}


}

$condominium = new Condominium("Enzo Condo",5,"Buendia Avenue, Makati City, Philippines");

//Mini Activity
//Create a pokemon constructor (5mins.)
	//properties:
	//type
	//level
	//trainer

	//methods:
	//attack
	//faint

//Instantiate 3 pokemon objects/instances

//Display the 3 objects in the webpage using var_dump


$pokemon1 = (object)[
	'type' => 'Bulbasaur',
	'level' => 1,
	'trainer' => "Ash"
];

$pokemon2 = (object)[
	'type' => 'Charmander',
	'level' => 2,
	'trainer' => "Ash"
];



class Pokemon {
   
   public $type;
   public $level;
   public $trainer;

   public function __construct($type,$level,$trainer){

   	$this->type = $type;
   	$this->level = $level;
   	$this->trainer = $trainer;

   }


   public function Attack() {
   		return "$this->name";
   }

   public function Faint() {
   		return "$this->name Faint";
   }

}

$pokemon3 = new Pokemon("Pikachu","trainer","Ash"];